# The Raven

## Task

- Read all the text from data/the-raven.txt.
- Count the frequency (how many times it's used) of each unique word.
- Write word+coun to CSV
- In Libre Calc or Excel: Create bar chart showing the frequency of the 10 most used words.

