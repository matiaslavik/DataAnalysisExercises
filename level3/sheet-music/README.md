# Hal Leonard sheet music

In this task you will download data about sheet music books from a website, and write it to a CSV.

## The dataset

A CSV containing the product ID of a number of sheet music books by Hal Leonard.

Each of these products can be found on www.halleonard.com.
We do however not have the product ID available, but it can be constructed like this: www.halleonard.com/PRODUCT_ID

## Task

- Read data/hal-leonard.csv (note that Title and Description is missing from all items!)
- Construct the URL for each item on www.halleonard.com (www.halleonard.com/PRODUCT_ID). Read the HTML and fetch the title and description of each of the products.
- Write the updated data back to CSV.
