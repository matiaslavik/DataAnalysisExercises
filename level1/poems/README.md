# Poems by author

## Task
The dataset data/poems.csv contains a list of poem names and their author, in the following format: "POEM_NAME by AUTHOR". Example "The Road Not Taken, by Robert Frost".

Using any tool you want, extract the atuhor name of each entry (the "by xxx" part) and move it to its own column, and write to a new CSV.

It should look like this.

The Road Not Taken; Robert Frost

The Raven; Edgar Allan Poe

...
