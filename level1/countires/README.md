# Country list to CSV

In this task you will convert a list of country names to a CSV.

## Task

Read data/countries.txt.

Create a new CSV that contains each country in a separate row entry.

In other words, it should look like this:

Denmark

Norway

Sweden

Germany

...

You're free to use any tool you want (Python, regular expressions search&replace, etc.)
